# Useful Scripts

This is a collection of useful scripts I use regularly at work and at my personal projects.

## ssh_save_to_authorized_keys.sh

Saves the host's ssh public key at a remote ssh server authorized_keys files, removing the need for entering the password in future connections.

To call this script from anywhere run:

``` bash
ln -s $PWD/ssh_save_to_authorized_keys.sh /usr/bin/ssh_save_to_authorized_keys
ssh_save_to_authorized_keys niceuser@192.168.0.12
```

## connect_bt.sh

Connect to a bluetooth device with a given MAC address.

Install expect dependency

``` bash
sudo apt install expect
```

Put your device on pairing mode then run.

``` bash
./connect_bt.sh CC:98:8B:D1:59:62
```

## connect_ps4_controller.py

Connect to a bluetooth PS4 Controller
Install expect dependency

``` bash
sudo apt install expect
sudo pip3 install pexpect
```

Put your controller on pairing mode then run.

``` bash
python3 connect_ps4_controller.py
```
