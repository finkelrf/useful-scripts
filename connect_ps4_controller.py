import pexpect as pex
import re

device_name = "Wireless Controller"

class Bt:
    def __init__(self):
        self.p = pex.spawn('bluetoothctl', encoding='utf-8')

    def remove_devices(self, device_name):
        print(f'Removing {device_name} if already paired')

        self.p.sendline('devices')
        done = False
        response = ''
        while not done:
            try:
                letter = self.p.read_nonblocking(1,1)
                response += letter
            except pex.exceptions.TIMEOUT:
                done = True

        devices = re.findall(r"Device (\w\w\:\w\w\:\w\w\:\w\w\:\w\w\:\w\w) ([\w -]*)", response)
        for device in devices:
            address, name = device
            if "Wireless Controller" == name:
                print(f'Removing {device_name}')
                self.p.sendline(f'remove {address}')

    def scan_for_device(self, device_name):
        self.empty_buffer()
        print('Starting scan')
        self.p.sendline('scan on')

        done = False
        line = ''
        dev_addr = []
        while not done:
            try:
                letter = self.p.read_nonblocking(1,10)
                line += letter
                if letter == '\n':
                    print(f'--------------line: {line}')
                    if 'Wireless Controller' in line:
                        dev_addr = re.search(r"\w\w\:\w\w\:\w\w\:\w\w\:\w\w\:\w\w", line).group()
                        return dev_addr
                    line = ''

            except pex.exceptions.TIMEOUT:
                done = True

        return ''

    def connect(self, device_address):
        self.empty_buffer()
        print('scan off')
        self.p.sendline('scan off')
        self.p.expect('#')
        print(f'connect {device_address}')
        self.p.sendline(f'connect {device_address}')
        # self.p.expect('#')
        self.empty_buffer(debug=True)


    def exit(self):
        print('Exit')
        self.p.sendline('exit')
        self.p.expect('#')

    def empty_buffer(self, debug=False):
        if debug:
            print('=========================================================')
        try:
            while True:
                l = self.p.read_nonblocking(timeout=1)
                if debug:
                    print(l, end='')
        except pex.exceptions.TIMEOUT:
            pass

        if debug:
            print('=========================================================')



if __name__ == "__main__":
    import subprocess

    subprocess.call(['sudo','systemctl','restart','bluetooth.service'])
    # device_address = 'A4:AE:11:44:A1:95'
    device_name = 'Wireless Controller'
    bt = Bt()
    bt.remove_devices(device_name)
    dev_addr = bt.scan_for_device(device_name)
    if dev_addr:
        bt.connect(dev_addr)
    bt.exit()

