# This script saves the local ssh public key to a remote ssh server, removing 
# the need to enter the password for future connections
if [ -z "$1" ]
then 
  echo "The first arg must be the ssh server user@ip.or.hostname"
else
  echo "Saving local ssh key to $1 authorized_keys"
  ssh $1 mkdir -p .ssh
  cat ~/.ssh/id_rsa.pub | ssh $1 'cat >> .ssh/authorized_keys'
  ssh $1
fi
